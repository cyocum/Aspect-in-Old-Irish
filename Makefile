all:
	xelatex aspect-in-old-irish.tex
	biber aspect-in-old-irish.bcf

clean:
	rm *.aux *.log *.pdf *.bcf *.blg *.bbl *.xml *-blx.bib *~