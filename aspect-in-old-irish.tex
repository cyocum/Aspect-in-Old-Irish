\documentclass[a4paper, 12pt, oneside]{article}

\usepackage{setspace}
\usepackage{fontspec}
\setmainfont[Mapping=tex-text]{Linux Libertine O}
\newfontfamily\nakula[Script=Devanagari]{Nakula}

\usepackage{microtype}
\usepackage{polyglossia}
\setmainlanguage[variant=british]{english}
\setotherlanguage[variant=anicent]{greek}
\setotherlanguage{sanskrit}
\setotherlanguage{german}

\usepackage[style=verbose-ibid, backend=biber]{biblatex}
\bibliography{aspect}

\author{Christopher Guy Yocum}

\title{Aspect in Old Irish: the Case of `ro-'}

\begin{document}

\maketitle

Aspect is a linguistic term which covers a variety of verbal sins.  Traditionally, verbs were analysed in terms of tense which provided an easy pedagogical guide for students.  However, starting with the introduction of Slavic Studies, the verbal landscape changed significantly.\footnote{\cite[3--65]{Binnick1991}.}  Slavic languages, unlike languages such as English, have a grammatical system of aspect in addition to tense.\footnote{\cite[441--451]{SussexCubberley2006}.}  This formal distinction caused a search for aspect in the verbal systems deriving from Proto-Indo-European (PIE).  This search provided a few tantalising clues as to the disposition of the verbal system in PIE.  In addition, it places aspect along side tense as a method for categorising verb morphology.

While this spurred research into the inner workings of this verbal category, it has not, however, reached a scholarly consensus.\footnote{\cite[257--271]{Bache1997}.}  The reason for this is the inherent complexity of the categories involved and the impreciseness of the definitions.  Although the complexity of a detailed analysis will inevitably cause disagreement among scholars, the stability of the verbal category in the face of many vastly differing modes of analysis establishes it as a firm verbal category.

In the study of Old Irish, however, verbal aspect is not well-known or well studied.\footnote{Although, there has been some work in Middle Welsh, see \cite{Falileyev1994}. Indeed, while covering other dimensions of aspect in Old Irish, Ailbhe Ó Corráin states that some of the topics discussed here do not exist in Old Irish, \cite[3--4]{OCorrain2008}.}  Thursyesen in \emph{A Grammar of Old Irish} (GOI) only mentions the connection between Old Irish (OI) `ro-' and Greek προ.  However, he does not extend the analysis to its natural conclusion as a form of verbal aspect.  In this paper, I want to show OI verbal prefix ro- is connected to the Greek preposition προ, Old Church Slavonic (OCS) aspectual prefix pro-,\footnote{\cite[70]{Lunt1966}.} and Sanskrit pra- ({\nakula प्र}) and how ro- has strong aspectual qualities in OI.

\section{Aspect in General Linguistics}

The definition by Comrie is considered the definitive definition of verbal aspect and is worth citing in full:

\begin{quotation}
Aspect is not concerned with relating the time of the situation to any other time-point, but rather with the internal temporal constituency of the one situation; one could state the difference as on e between situation-internal time (aspect) and situation-external time (tense).\footnote{\cite[5]{Comrie1976}.}
\end{quotation}

While this is taken as definitive, it does not convey all the possible meanings of the term aspect. Specifically, aspect is bifurcated by linguists into to groups `grammatical aspect' and \textgerman{\emph{Aktionsarten}}.  The first is derived from the prefix aspectual markers in OCS where aspect is grammatically defined (perfective/imperfective pairs of verbs).\footnote{insert footnote here}  The second comes from linguists attempts at categorizing aspect as applied to other Western European languages.  Many have attempted to list all the categories of \textgerman{\emph{Aktionsarten}} but a definitive list, as is similar to much in aspectology, has yet to reach a scholarly concensus.  However, a good working list can be created without offending too many:

\begin{itemize}

\item momentaneous
\item durative
\item inchoative, ingressive
\item iterative [frequentive]
\item progressive
\item perfective
\item imperfective
\item proximity in the future
\item proximity in the past\footnote{\cite[146]{Binnick1991}.}
\item gnomic imperfective\footnote{\cite[13--17]{BertinettoLenci2010}.} (also known as the gnomic present)\footnote{\cite[247]{Binnick1991}.}
\item accomplishment
\item achievement
\item activity
\item performance
\item series
\item state\footnote{\cite[170--179]{Binnick1991}.}

\end{itemize}

As one can see, \textgerman{\emph{Aktionsarten}} cover an extermely wide semantic range.  So, for the purposes of this study, only `grammatic aspect' which has morphological and grammatical meaning will be examined.  \textgerman{\emph{Aktionsarten}} will be discussed only when it has direct bearing on the meaning of a particular word form.

\subsection{Aspect From PIE to Proto-Slavic and OCS}

To give the reader an appreication for grammatical aspect and how it works, the aspectual system of OCS will be presented.  The verbal system of PIE was strongly aspectual (and it could be argued that it is an aspect-oriented language).\footnote{\cite[121--132]{Bhat1999}.  See also, \cite[139--151]{Lehmann1974}.}  Proto-Slavic inhereting both the aspect system of early PIE and the tense system of later PIE.  The thematic vowels merged with the inflectional endings.  This development caused instability in the system.  For Proto-Slavic and OCS, the speakers regularized the entire verb system adding aspectual pairs: Perfective and Imperfective.\footnote{\cite[11--6]{Migdalskim2006}.  See also, \cite[82--102]{HewsonBubenik1997}.} This distinction is generally shown in a variety of ways:

\begin{description}
\item[prefixes] tvoriti ``do'' / sъtvoriti ``to have done''
\item[thematic suffix] stõp-aj-õtъ ``to tread'' / stõp-i-ti ``to have trodden``
\item[thematic suffix plus root modification] prašt-aj-õtъ ``to forgive'' / prost-i-ti ``to have forgiven''
\item[suppletion] glagola-ti ``to say'' / rek-õtъ ``to have said''\footnote{\cite[74]{Lunt1966}; \cite[18--9]{Migdalskim2006}}
\end{description}

The system as it appears in a modern East Slavic languages, such as Russian, which shows only prefixation to show pefectives, was still undergoing transformation at the time of OCS.\footnote{\cite[37]{Migdalskim2006}.}

\section{`Ro-' as Aspectual Marker}

As seen in OCS, prefixing was one of the ealier forms of formally marking aspect.  In OCS, pro- was one of the prefixes used for marking perfectivity.  To see the development of `ro-' one only needs to look at its close cousins.  The loss of initial p- in Proto-Celtic (PC) renders this simple. PC *pro- aligns directly with Sanskrit pra- ({\nakula प्र}), OCS pro-, and Greek προ.  Thus, while Sanskrit and Greek weaken or lose the grammatical aspect of *pro-, PC and OCS retain it.

\subsection{`Ro' in GOI}

In GOI Thurnysen establishes the basis for OI and its relationship to PIE.  The particle `ro-' is singled out for explicit discussion as it has special meanings in terms of verbs in OI.  In the case of `ro-' Thurneysen identifies two kinds: fixed `ro-' and movable `ro-'.  For our purposes, movable `ro-' will be the category under discussion.  When discussing movable `ro-', Thurneysen briefly mentions the Greek preposition προ.  He then lists the various functions of `ro-'.

\begin{itemize}
\item It indicates that an act or state is perfect.
\item It gives perfective force to the preterite indicative and past subjunctive.
\item With the imperfect it denotes action repeatedly completed in past time.
\item With the present indicative and subjective it denotes action which has been completed at the time that another action takes place.
\item With present subjuntive used as the future, it denotes action which as been completed at the time that another action takes place (\emph{futurum exactum}).
\item It expresses possbility or ability.
\item Converted horative subjunctive into an opative.
\item It is used in gnomic literature in a universal sense using with `ro-' prefixed to the present tense.\footnote{\cite[341--343]{GOI}.}
\end{itemize}

\subsection{`Ro-' in The Early Irish Verb}

At this point, the aspect in Old Irish verbs stayed until Kim McCone took up the question in \emph{The Early Irish Verb}.\footnote{\cite{McCone1997}.}  In this magisterial study of the early Irish Verb, he terms certain prefixes as \emph{augments}, drawing upon their discussion in the grammar of Ancient Greek.  Although, he disallows a similarity other than the use of the word.\footnote{\cite[91]{McCone1997}.}  Within this augment scheme, he places most Old Irish prefixes, including `ro-'.

McCone describes aspect in a relatively cursory way.\footnote{\cite[111--126]{McCone1997}.}  Aspect is either perfecitve or imperfective (without stating the Slavic connection at first use). \textgerman{\emph{Aktionsarten}} are described as \emph{modes} with `\ldots{}no agreed limit on the number of modes of action to be employed according to circumstances and no restriction of them to the lexical sphere only.'  Moreover, he then picks two terms \emph{iterative} and \emph{durative} with no explaination as to why these should be chosen over any other \textgerman{\emph{Aktionsarten}}.  Last, but not least, he picks the term \emph{telic} and \emph{atelic} to use, again without explanation.\footnote{\cite[114--121]{McCone1997}.} To encompass the possibility/probablity function of `ro-', he uses the telicity.\footnote{\cite[119--119]{McCone1997}.}  Finally, he states:

\begin{quote}

In response to the apparent hopelessness of the quest for a functionally oriented term capable of covering all major grammatical uses of \emph{ro} and equivalents, it has been decided in the present work to opt for `augment' as a purely formal designation of the elements in question and to prefix `augmented' to any tense or mood combined with them\ldots{}\footnote{\cite[125]{McCone1997}.}

\end{quote}

To recap, in McCone's view, `ro-' imparts three states upon its verbal charge in OI: 

\begin{itemize}
\item Perfect/Imperfect
\item Iterative/Durative
\item Telic/Atelic
\end{itemize}

\section{`Ro-' in Tartessian}

As we can see from the Slavic evidence, speakers in Proto-Slavic and OSC made a choice to continue to use the aspectual system inhereted from PIE.  This caused the grammaticallization of aspect to a high degree.  The question then is: how did this develop in Proto-Celtic?

While it would be easy to dismiss `ro-' in Old Irish without outside comperanda, it is now less likely.  Tartessian, the Celtic language of southern Spain and portugal, also shows a reflex of `ro-' from outside the Insular context.  The identification of Tartessian from the gravestones in and around Tartessos (\textgreek{Ταρτησσός}) was made by Prof. John Koch.\footnote{\cite{Koch2009}.  For a dissenting point of view, see \cite{Zeidler2011}.}  In subesquent work, he extends his anyalsis and places Tartessian to the years 700--500 BC.\footnote{\cite[7]{Koch2011}.}  In this investigation, he has identified an analogous use of `ro-'.  The proto-typical example is the phrase t\textsuperscript{e}e-ro·b\textsuperscript{a}are (b\textsuperscript{a}a) naŕk\textsuperscript{e}ent\textsuperscript{i}i ([the deity \emph{or} this burial (\emph{not} this inscribed stone)] has carried away ?so they now remain unmoving).\footnote{\cite[101--2]{Koch2011}.}  One can immediately see that t\textsuperscript{e}e-ro·b\textsuperscript{a}are is extremely similar to Old Irish `ro-' as the marker of the perfect.  In addition there is tantilizing evidence from the corpus as it now stand that preverbal `ro-' actively stops the use of PIE perfect stem with active ending -ii in verbs.  For example, ro-n·b\textsuperscript{a}aren versus t\textsuperscript{e}ee·b\textsuperscript{a}arent\textsuperscript{i}i.\footnote{\cite[109--110]{Koch2011}.}

If the dates for Tartessian are accepted, this places the development of `ro-' possibly in the Proto-Celtic stage of the language.  This would mean that at least `ro-' as inherited by Old Irish and Welsh would have the force of perfect marking.  The other alternative is that `ro-' was developed independently in both languages.  However, given that it shows similar usage in both Old Irish and Welsh and has similar uses in other IE languages, is a remote possibility. 

In conclusion, the perfect marking of the verb by `ro-' and the active blocking of the active ending of the perfect stem in Tartessian shows that `ro-' was solidily within the Proto-Celtic sphere of development.  However, without more evidence from Tartessian as to the other particularities of `ro-', it is hard to say wether the aspectual distinctions were an Insular Celtic innovation or someting inherited from Proto-Celtic.

\subsection{Pra- ({\nakula प्र}) in Sanskrit}

According to Macdonell's \emph{A Practical Sanskrit Dictionary} the prefix pra- ({\nakula प्र}) means `ad. (with verbs) before; forward, onward, on, forth; -with nouns, fore-; great (in relationship); -with a. exceedingly, very.'\footnote{\cite[171]{Macdonell1929}.}  This parallels closely with Old Irish's use of `ro-' with both verbs and nouns.  For example, with nouns \emph{pra-pautra} `great-grandson' ({\nakula प्रपौत्र}), \emph{pra-veka} `choice, chief, exquisite, most excellent' ({\nakula प्रवेक}), or \emph{prakhara} `very hot' ({\nakula प्रखर})\footnote{\cite[176]{Macdonell1929}.}  For example, in verbs \emph{prakṣēpḥ} ({\nakula प्रक्षेप}) `throwing forward, projecting' or \emph{pracyu} ({\nakula प्रच्यु}) `to move away, go away, withdraw retreat'.\footnote{For a fuller discussion of Sanskrit, see \cite[46--66]{HewsonBubenik1997}.}

\subsection{Pro- (πρό-) in Greek}

According to Liddle and Scott's \emph{A Greek--English Lexicon}, πρό- as an adverbial of time can mean `before' or `earlier'.  For example, `\textgreek{πρό οἱ εἴπομεν}' (Od. 1.37) or `\textgreek{τά τ᾽ ἐσσόμενα πρό τ᾽ ἐόντα}' (Hes. Th. 32,38).  πρό- in composition with verbs can mean `before, beforehand'.  For example, \textgreek{προαισθάνομαι} `perceive or observe beforehand' (Id. 3.38) or \textgreek{προγίγνομαι} `to be born before, exist before' (Hdt. 7.3).\footnote{For a fuller discussion of Ancient Greek, see \cite[24--45]{HewsonBubenik1997}.}

\section{Aspect Reclaimed}

There are many problems with McCone's treatment of aspect in the Old Irish verb.  Not the least of which is that he does not cite any secondary linguistic literature on aspect in his explanations.  The use of telicity to mark potential action seems rather convoluted from the point of view of general linguistic aspectology.

As one can see clearly, movable `ro-' coincides with nearly identically with both OCS pro- and the list of aspectual categories listed earlier.  For instance, changing the simple past verb into a perfect past.  In addition, it also has some characteristics of \textgerman{\emph{Aktionsarten}}, for instance, gnomic statements.  These coincidences strongly suggest that `ro-' is an aspect marking verbal operator on the same order as OCS pro-.

There is one category of `ro-' which does not directly fit the analysis above.  this is the `ro- of possibility or ability'.  This can be incorporated into the analysis by demonstrating that while the present subjunctive to which `ro-' is affixed gives a virtual character to the verb, the accomplishment, achievement, activity, and performance categories of verbal aspect combined with the virtual character of the subjunctive can equate to possibility or ability.  Here, tense and aspect work in concert to create a productive combination.

In terms of Thurneysen's anlysis of `ro-', at the risk of bringing all the baggage of aspect, the special case of `ro-' could have been discribed more succinctly and more convincingly as a marker of aspect rather than a special case.  As it stands, `ro-' rather than being a degenerate case of PIE preposition *pro, `ro-' is a aspect modifying operator whose purpose was to mark aspect for the listener/reader of Old Irish.

\section{Conclusion}

While aspect and its study have spawned many valient attempts to define what exactly aspect is, it is undoubtly a integral part of the verbal complex in all PIE languages.  In this, the Celtic Verb is no different.  However, unlike English which has a very diffuse aspectual system, Old Irish and Welsh have grammaticalized it with the use of the preverb `ro-' in much the same way as East Slavic.

As we have seen, while Thurnysen was undoubtedly correct in this analysis of `ro-', he missed its aspectual component and how it fit into the wider scheme of lingustic aspect.  While this is noticed and further commentated on by McCone, his anaylisis is lacking in vigour and placing his theory of aspect for the Old Irish verb in wider linguistic scholarship.

Of course, as with so much of Celtic Stuides, the study of aspect in Celtic verbs is still in its infancy with much more still to discover.

\printbibliography

\end{document}
